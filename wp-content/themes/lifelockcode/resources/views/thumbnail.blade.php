@if(is_singular())
	<div class="post-thumbnail">
		{!! the_post_thumbnail() !!}
	</div>
@else
	<a class="post-thumbnail" href="<?php the_permalink();?>" aria-hidden="true">
		{!! the_post_thumbnail('post-thumbnail', array('alt' => get_the_title())) !!}
	</a>
@endif