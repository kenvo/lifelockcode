
<?php
/*
Template Name: test
*/?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>jQuery Show Hide Using Select Box</title>
<style type="text/css">
    .box{
        padding: 20px;
        display: none;
        margin-top: 20px;
        border: 1px solid #000;
    }
    .red{ background: #ff0000; }
    .green{ background: #00ff00; }
    .blue{ background: #0000ff; }
</style>
<script type="text/javascript" src="http://code.jquery.com/jquery.min.js"></script>
<script type="text/javascript">
$(document).ready(function(){
    $("select").change(function(){
        $(this).find("option:selected").each(function(){
            if($(this).attr("value")=="red"){
                $(".box").not(".red").hide();
                $(".red").show();
            }
            else if($(this).attr("value")=="green"){
                $(".box").not(".green").hide();
                $(".green").show();
            }
            else if($(this).attr("value")=="blue"){
                $(".box").not(".blue").hide();
                $(".blue").show();
            }
            else{
                $(".box").hide();
            }
        });
    }).change();
});
</script>
</head>
<body>
    <div>
        <select>
            <option>Choose Color</option>
            <option value="red">Red</option>
            <option value="green">Green</option>
            <option value="blue">Blue</option>
        </select>
    </div>
    <div>
        <ul class="dropdown-menu">
                    <li><a href="#">LLC3015 - 15% Off + 30 Days Risk Free*</a></li>
                    <li><a href="#">LLC1030 - 10% Off + 30 Days Risk Free</a></li>
                    <li><a href="#">LLCODE2 - 60 Days Risk Free* </a></li>
                    <li><a href="#">LLCODE3 - 10% Off Any Product</a></li>
                    <li><a href="#">LLCODE4 - 30 Days Risk Free*</a></li>
                    <li><a href="#">No Promo Code</a></li>
                </ul>
    </div>

    <div class="ricing-header red box" style="text-align: center; background-color: #737070;">
                        <h3 style="color: #fff; margin-bottom: 0px;" class="month1">$8.49/month</h3>
                        <h3 style="color: #fff;" class="annual1">$93.41/annual</h3>
                        <a href="http://lifelocktrk.com/?a=107&amp;c=28&amp;s1=dmn&amp;s2=llcodes&amp;s3=LLC3015&amp;s4=&#10;u&#9;&#9;&#9; " class="pri-a" title="Enroll Button 1"><img src="http://lifelockcode.vicoders.com/wp-content/uploads/2016/12/enroll-button-1.png" class="btn-img-hover" alt="Life Lock promo code"><br></a> <!--<h4><em>Enroll Now: Get 15% Off + 30 Days Risk Free*</em><br />(Price includes 15% discount)</h4>--></div>

    <div class="ricing-header green box" style="text-align: center; background-color: #737070;">
                        <h3 style="color: #fff; margin-bottom: 0px;" class="month1">$8.49/month</h3>
                        <h3 style="color: #fff;" class="annual1">$94.41/annual</h3>
                        <a href="http://lifelocktrk.com/?a=107&amp;c=28&amp;s1=dmn&amp;s2=llcodes&amp;s3=LLC3015&amp;s4=&#10;u&#9;&#9;&#9; " class="pri-a" title="Enroll Button 1"><img src="http://lifelockcode.vicoders.com/wp-content/uploads/2016/12/enroll-button-1.png" class="btn-img-hover" alt="Life Lock promo code"><br></a> <!--<h4><em>Enroll Now: Get 15% Off + 30 Days Risk Free*</em><br />(Price includes 15% discount)</h4>--></div>

    <div class="ricing-header blue box" style="text-align: center; background-color: #737070;">
                        <h3 style="color: #fff; margin-bottom: 0px;" class="month1">$8.49/month</h3>
                        <h3 style="color: #fff;" class="annual1">$95.41/annual</h3>
                        <a href="http://lifelocktrk.com/?a=107&amp;c=28&amp;s1=dmn&amp;s2=llcodes&amp;s3=LLC3015&amp;s4=&#10;u&#9;&#9;&#9; " class="pri-a" title="Enroll Button 1"><img src="http://lifelockcode.vicoders.com/wp-content/uploads/2016/12/enroll-button-1.png" class="btn-img-hover" alt="Life Lock promo code"><br></a> <!--<h4><em>Enroll Now: Get 15% Off + 30 Days Risk Free*</em><br />(Price includes 15% discount)</h4>--></div>
</body>
</html>                                		