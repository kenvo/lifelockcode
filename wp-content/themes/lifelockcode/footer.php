<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the "site-content" div and all content after.
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since lifelockcode 1.0
 */
?>

	</div><!-- .site-content -->

	<footer id="colophon" class="site-footer" role="contentinfo">
		<div class="footercontainer">
			<div class="site-info footerBot">
			<?php dynamic_sidebar( 'footer-bot' ); ?>
				<p style="font-size: 10pt;font-family: helvetica;">Copyright <?php echo date_i18n('Y'); ?> | llpromocodes all rights reserved</p>
				<a data-mini="true" target="_self" data-role="button" data-theme="d" data-icon="gear" href="http://lifelockcode.local/?jtpl=11" class="switchDesktop ui-btn ui-shadow ui-btn-corner-all ui-mini ui-btn-icon-left ui-btn-up-d" data-corners="true" data-shadow="true" data-iconshadow="true" data-wrapperels="span" rel="nofollow" title="View Mobile Version&amp;nbsp;"><span class="ui-btn-inner"><span class="ui-btn-text">View Mobile Version</span><span class="ui-icon ui-icon-gear ui-icon-shadow">&nbsp;</span></span></a>
			</div><!-- .site-info -->
			<div class="footerTop justify contaicner">
				<div class="col-md-3">
					<?php dynamic_sidebar( 'footer-1' ); ?>
				</div>
				<div class="col-md-3">
					<?php dynamic_sidebar( 'footer-2' ); ?>
				</div>
				<div class="col-md-3">
					<?php dynamic_sidebar( 'footer-3' ); ?>
				</div>
				<div class="col-md-3">
					<?php dynamic_sidebar( 'footer-4' ); ?>
				</div>
			
			</div>
		</div>
	</footer><!-- .site-footer -->

</div><!-- .site -->

<?php wp_footer(); ?>
	<script>
		jQuery(document).ready(function($) {
			function scrollNav() {
			  $('header .container .logo .widget .custom h2 span a').click(function(){  
			    //Toggle Cl
			    //Animate
			    $('html, body').stop().animate({
			        scrollTop: $( $(this).attr('data-href') ).offset().top - 160
			    }, 400);
			    return false;
			  });
			  // $('.scrollTop a').scrollTop();
			}
			scrollNav();
		});
	</script>
</body>

</html>
