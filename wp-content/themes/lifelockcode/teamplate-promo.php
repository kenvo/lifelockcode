<?php 
/*
 Template Name: promo
*/
get_header(); ?>
    <div id="primary">
        <div id="content" role="main">
            <?php while ( have_posts() ) : the_post(); ?>
            <?php  
                $id_page = get_the_ID();
                $id_code = '134';
                $get_post_code = new WP_Query( array( 'post_type' => 'Code','posts_per_page'=> 1,) );
                while ( $get_post_code->have_posts() ) : $get_post_code->the_post();
                    $id_code = get_the_ID();
                endwhile;
                $max_discount = get_post_meta($id_code, 'discount', true);
                $max_term = get_post_meta($id_code, 'term', true);
            ?>
            <div class="section1 promo col-sm-12 col-xs-12 promo-mobile">
                <div class="container">
                    <div class="promo-top">
                        <div class="w1160 autoWidth">
                            <div class="homeTopLeft fl col-md-8 col-sm-12 col-xs-12">
                                <div class="custom">
                                    <div class="mascot">

                                        <div class="images_promo">
                                            <a href="http://lifelocktrk.com/?a=107&c=12&s1=dmn&s2=llpc&s3=LLC1030&s5=ll_logo"><img class="img-responsive" src="<?php bloginfo('template_directory'); ?>/image/LL_Corp_H_4C.png"></a>
                                        </div>

                                        <div class="content">
                                                <div class="box autoWidth">
                                                
                                                <div id="shield_promo" class="fr shield shield2" data="<?php echo render_url($id_code, $id_page); ?>">
                                                        <h3 style="text-align: center;">
                                                        <strong><span class="title_shield" style="font-size: 15pt;">BEST OFFER:</span></strong></h3>
                                                        <div id="cl-effect-5" class="cl-effect-5">
                                                            <h2>
                                                            <a href="<?php echo render_url($id_code, $id_page); ?>" title="LLC3015">    <span style="color: #ffff66;" data-hover="LLC1030"><?php the_field('code',$id_code); ?>
                                                                 </span>
                                                            </a>
                                                            </h2>
                                                        </div>
                                                        <p class="text_shield" style="text-align: center;line-height: 20px;">
                                                            <strong>
                                                                <span class="ten" style="font-size: 18pt;"><span class="up_to"><?php echo $max_discount; ?>% off</span></span>
                                                            </strong>
                                                            <strong>
                                                                <span class="textshield" style="font-size: 18pt;">first year* <br> +  Free <br> Shredder</span>
                                                            </strong>
                                                        </p>
                                                    </div>

                                                    <div class="boxCont">
                                                        <h2><a href="<?php echo render_url($id_code, $id_page); ?>" title="CHOOSE PROMO CODE &amp;amp;SAVE AT LIFELOCK.COM">CHOOSE PROMO CODE &amp;<span style="font-size: 13pt;"><br>SAVE AT <strong>LIFELOCK.COM</strong></span></a></h2>
                                                        <br>
                                                        <ul>
                                                            <li>
                                                                <p>LifeLock Identity Alert<sup>®</sup> System†</p>
                                                            </li>
                                                            <li>
                                                                <p>Lost Wallet Protection</p>
                                                            </li>
                                                            <li>
                                                                <p>Address Change Verification</p>
                                                            </li>
                                                            <li>
                                                                <p>Reduced Pre-Approved Credit Card Offers</p>
                                                            </li>
                                                            <li>
                                                                <p>24/7 Live Member Support</p>
                                                            </li>
                                                            <li>
                                                                <p>Million Dollar Protection™ Package†††<br>... and More!</p>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                    <div class="holder"><img src="<?php bloginfo('template_directory'); ?>/image/layer1-hand.png" alt="LifeLock coupons"></div>
                                                </div>
                                        </div>
                                    </div>
                                </div>

                            </div>

                            <div class="custom col-md-4 col-sm-12 col-xs-12 top-mobile-35">
                                <div class="homeTopRight">
                                    <h4>LifeLock<sup>®</sup></h4>
                                    <h2 class="red">Promo</h2>
                                    <h2 class="black">Codes</h2>
                                    <p>LifeLockCodes.com is your source for the best LifeLock promo codes. Save on identity theft protection with one of the LifeLock promotional codes below. Each month we provide up to 5 great offers. Get your LifeLock discount by simply clicking on the code or just type the code into your enrollment form. The only thing smarter than protecting your identity is saving money while doing it! <a class="promo-learn-more" href="http://www.lifelockcodes.com/what-is-lifelock" title="Learn more">Learn more</a></p>
                                    <h2 class="red"><a href="<?php echo render_url($id_code, $id_page); ?>" title="Enroll Now">Enroll Now</a></h2>
                                    <h2 class="red"><a href="/pricing" title="Get Pricing">Get Pricing</a></h2>
                                </div>
                                <div style="position: absolute; left: -40px; top: -25px; width: 1px; height: 1px; overflow: hidden;" data-mce-bogus="1" class="mcePaste" id="_mcePaste"><span style="font-size: 14.0pt;">®</span></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="section2 promo-mobile-section2">
                <div class="container">
                    <?php 
                        $loop = new WP_Query( array( 'post_type' => 'Code','posts_per_page'=> 5,) );
                        $i=1;
                        while ( $loop->have_posts() ) : 
                            $loop->the_post();
                            if($i == 1) {
                                $class_sd = 'sdone';
                            } elseif ($i == 2 ) {
                                $class_sd = 'sdtwo';
                            } elseif ($i == 3) {
                                $class_sd = 'sdthree';
                            } elseif ($i == 4) {
                                $class_sd = 'sdfour';
                            } elseif ($i == 5) {
                                $class_sd = 'sdfive';
                            } else {
                                $class_sd = 'sdfive';
                            }
                    ?>
                        <div class="sheld <?php echo $class_sd; ?> sheld_child_promo" data_child_promo="<?php echo render_url(get_the_ID(), $id_page); ?>">
                            <div class="content-shel">
                                <p>
                                    <?php the_field('name',get_the_ID()); ?>
                                </p>
                                <p>
                                    <a href="<?php echo render_url(get_the_ID(), $id_page); ?>">
                                        <?php the_field('code',get_the_ID()); ?>
                                    </a>
                                    <?php the_field('promotion',get_the_ID()); ?>
                                </p>
                            </div>
                        </div>
                    <?php
                            $i++;
                        endwhile;
                    ?>
                </div>
            </div>
        </div>

        <div class="text_bottom_home">
            <p>*Terms Apply</p>
        </div>

        <div class="section3">
            <div class="container">
                <?php the_field('home-section3','19'); ?>
            </div>
        </div>

        <div class="section4">
            <div class="container">
                <?php the_field('home-section4','19'); ?>
            </div>
        </div>
    </div>

    <?php endwhile; // end of the loop. ?>

        </div>
        <!-- #content -->
        </div>
        <!-- #primary -->

        <?php get_footer(); ?>