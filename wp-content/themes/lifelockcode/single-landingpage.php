<?php
/**
 * WP Post Template: landing-page
 */
get_header();?>
	<!-- Not move -->
	<style type="text/css">
		#k2Container {
			display: none ;
		}
		.content-area {
			margin-top: 0px !important;
			margin-bottom: 0px !important;
			padding: 0px !important;
		}
		#content #primary .warning {
			display: none !important;
		}
		#content #primary .container {
			padding: 0px !important;
		}
		.homeTopLeft.fr.re_fl > .custom .box.autoWidth {
			margin-left: 0px !important;
		}
		.homeTopLeft .mascot {
			background-position: 35% 110%;
		}
		.landing-content-page .homeTopLeft .shield {
			margin-top: 50px;
			color: #fff;
		}
		.landing-content-page .homeTopLeft .shield .info_code {
			font-size: 16pt;
			margin-left: 15px;
		}
		.landing-content-page .holder {
			top: 39px !important;
		}
		.landing-content-page .landing-content{
			margin-top: 45px;
		}
		.landing-content .homeTopRight .red .button {
			padding-left: 20px;
		}
		.landing-content .content-landingpage {
			font-family: Arial;
		}
		.landing-content .content-landingpage .sanji-title {
			line-height: 20px;
			text-align: center;
			margin-bottom: 70px;
		}
		.homeTopLeft .holder {
			left: -31px !important;
		}
		.landing-content .landing-leftside, .landing-content .landing-rightside {
			margin-top: 35px;
		}
		@media only screen and (min-width: 481px) and (max-width: 1023px) {
			.landing-content-page .homeTopLeft .box {
				width: 83% !important;
			}
			.landing-content-page .homeTopLeft .shield {
				padding: 30px 105px 55px !important;
				color: #fff;
				background-size: 56%;
			}
			.homeTopLeft .mascot {
				background-size: 28%;
				background-position: 38% 60% !important;
			}
			.homeTopLeft .holder {
				left: 68% !important; 
			}
		}
		@media only screen and (max-width: 480px) {
			.landing-content-page .landing-content .col-md-7 h1 {
				line-height: 50px;
			}
			.homeTopLeft .mascot {
				background: none !important;
			}
			.landing-content-page .homeTopLeft .box {
				width: 100% !important;
			}
			.landing-content-page .homeTopLeft .shield {
				max-width: 480px;
				width: 100%;
				padding: 67px 72px 55px !important;
				color: #fff;
			}
			.landing-content-page .homeTopLeft .shield .info_code {
				margin-left: 25px !important;
    			margin-top: 10px;
			}
		}
	</style>
		<?php  
            $id_page = get_the_ID();
            $get_post_code = new WP_Query( array( 'post_type' => 'Code','posts_per_page'=> 1,) );
            if(!empty($get_post_code)) {
            	$id_code = $get_post_code->posts[0]->ID;
            } else {
            	$id_code = '134';
            }
            $max_discount = get_post_meta($id_code, 'discount', true);
            $max_term = get_post_meta($id_code, 'term', true);
        ?>
		<div class="container landing-content-page">
			<div class="lan-thumnai">
				<?php echo get_the_post_thumbnail($id_page, 'full'); ?>
			</div>
			<div class="landing-content">
				<div class="col-md-3 col-sm-12 col-xs-12 landing-leftside">
					<div class="moduletable">
						<div class="homeTopLeft fr re_fl">
                        <div class="custom">
                            <div class="mascot">
                                <div class="content">
                                    <div class="box autoWidth">
                                        <div class="fr shield">
                                            <h3 style="text-align: center;"><strong><span style="font-size: 12pt;">THE #1 LIFELOCK PROMO CODE:</span></strong></h3>
                                            <div id="cl-effect-5" class="cl-effect-5">
                                                <h2>
                                                <a href="<?php echo render_url($id_code, $id_page); ?>">
                                                    <span style="color: #ffff66;" data-hover="LLC3015"> <?php the_field('code', $id_code); ?></span>
                                                </a>
                                                </h2>
                                            </div>
                                            <p class="info_code"><strong >
                                             <?php echo $max_discount; ?>% Off + <?php echo $max_term; ?> Days Risk Free*</strong>
                                            </p>
                                        </div>
                                        <div class="holder"><img src="<?php bloginfo('template_directory'); ?>/image/layer1-hand.png" alt="Life Lock promo code" /></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
					</div>
				</div>
				<div class="col-md-7 col-sm-12 col-xs-12 content-landingpage">
					<?php the_content();?>
				</div>
				<div class="col-md-2 col-sm-12 col-xs-12 landing-rightside">
                    <div class="homeTopRight">
                        <h2 class="red"><a class="button" title="Enroll using Promo Code" href="<?php echo render_url($id_code, $id_page); ?>">Enroll using Promo Code</a></h2>
                        <h2 class="red"><a class="button" title="Get Pricing" href="/pricing">Get Pricing</a></h2></div>
                    </div>
				</div>
			</div>
		</div>
	</div>
</div>

 <?php get_footer()?>