<?php
/*
Template Name: Products
*/
get_header(); ?>

<?php while ( have_posts() ) : the_post(); $title = get_the_title(); ?>
	<?php
		$id_page = get_the_ID();
		$id_code = '134';
		$get_post_code = new WP_Query( array( 'post_type' => 'Code','posts_per_page'=> 1,) );
		while ( $get_post_code->have_posts() ) : $get_post_code->the_post();
			$id_code = get_the_ID();
		endwhile;

		$arr_infoproduct = [];
		$info_products = new WP_Query( array( 'post_type' => 'info_products','posts_per_page'=> -1,) ); 
	    while($info_products->have_posts() ) : 
	        $info_products->the_post();
	        $arr_infoproduct[get_the_title()]['id'] = get_the_ID();
	        $arr_infoproduct[get_the_title()]['option'] = get_field('option', get_the_ID());
	    endwhile;
		$max_discount = get_post_meta($id_code, 'discount', true);
		$max_term = get_post_meta($id_code, 'term', true);
	?>
<?php //the_content(); ?>
<div class="innerCont w1354 kenproducts">
	<div class="largeTitle">
		<h1><?php echo $title; ?></h1>
	</div>
	<div class="box-shadow" style="font-family: Arial, helvetica, sans-serif; margin:0 0px; font-size: 11pt; color: #6c6060;">
		<h2>LifeLock Plan Options</h2>
		<p><strong>LifeLock Individual Plans: </strong><br><strong>Identity Theft Protection Services </strong><br><strong>We do more to protect your credit and your good name.</strong><br><br><br></p>

		<p style="font-size: 13pt !important;"><span style="font-size: 1.17em; font-family: 'Open Sans', sans-serif;" class="pri-title">Prices Include Promo Code: <a href="<?php echo render_url($id_code, $id_page); ?>" style="corlor: #333;" title="<?php the_field('code',$id_code); ?>"><?php the_field('code',$id_code); ?></a> (Save <?php echo $max_discount; ?>% off first year*)</span></p>
		<div class="garung_tab_mobile">
			<div class="container">
				<div class="row">
					<!-- <div class="span12"> -->
			            <div id="tab" class="btn-group" data-toggle="buttons-radio">
			              <a class="btn active garung_tab_a" data-num="1" data-toggle="tab">Standard</a>
			              <a class="btn garung_tab_a" data-num="2" data-toggle="tab">Advance</a>
			              <a class="btn garung_tab_a" data-num="3" data-toggle="tab">Ultimate Plus</a>
			              <a class="btn garung_tab_a" data-num="4" data-toggle="tab">Junior</a>
			            </div>
			        <!-- </div> -->
			    </div>
			</div>
		</div>
		<!--Features-->
		<div style="background-color: #fffefe; padding: 0px 10px; width: 20%; height: 92%; float: left; font-family: Arial, Helvetica, sans-serif; font-size: 9pt; color: #6c6060;" class="garung_plan_feature">
			<div class="header-more" style="height: 120px; padding-top: 20px; text-align: center;">
				<h2>Plan Features</h2>
			</div>
			<!-- <div class="body-more" style="height: 270px;">&nbsp;</div> -->
			<div class="footer-more" style="padding-top: 295px;">
				<ul id="feature" class="box-title">
					<?php
	                    $get_checkbox = get_field_object('option');
	                    if( $get_checkbox ): ?>
	                        <?php 
	                        	$i = 0;
	                        	foreach( $get_checkbox['choices'] as $color ): 
	                        		if(($i % 2) == 0):
	                        ?>
	                            		<li style="background-color: #f0ebeb; padding: 5px 5px; height: 30px;"><?php echo $color; ?></li>
	                        <?php 
	                        		else:
	                        			if ($i == 9)
	                        ?>
	                    				<li style="padding: 5px 5px; height: 30px;"><?php echo $color; ?></li>
	                    	<?php
	                    			endif;
	                    			$i++;
	                        	endforeach; 
	                    endif;
                    ?>
				</ul>
			</div>
		</div>
		<?php 
			$i = 1;
			$loop = new WP_Query( array( 'post_type' => 'info_products','posts_per_page'=> -1,'orderby'   => 'id','order' => 'ASC',) ); 
			while($loop->have_posts() ) : $loop->the_post();
				$price_month = (float)get_field('price_month', get_the_ID());
				$price_annual = (float)get_field('price_annual', get_the_ID());

				$month = round(($price_month * ((100-$max_discount)/100)), 2);
				$annual = round(($price_annual * ((100-$max_discount)/100)), 2);
				$annual2 = number_format((float)$annual, 2, '.', '');

				if($i == 1) {
					$month = round(($price_month * ((100-$max_discount)/100)), 2);
					$annual = round(($price_annual * ((100-$max_discount)/100)), 2);
					$annual2 = number_format((float)$annual, 2, '.', '');
					$background = '#fcfafa';
				} elseif ($i == 2) {
					$month = round(($price_month * ((100-10)/100)), 2);
					$annual = round(($price_annual * ((100-10)/100)), 2);
					$annual2 = number_format((float)$annual, 2, '.', '');
					$background = '#f4f2f2';
				} elseif($i == 3) {
					$month = round(($price_month * ((100-10)/100)), 2);
					$annual = round(($price_annual * ((100-10)/100)), 2);
					$annual2 = number_format((float)$annual, 2, '.', '');
					$background = '#e5e1e1';
				} else {
					$month = round(($price_month * ((100-10)/100)), 2);
					$annual = round(($price_annual * ((100-10)/100)), 2);
					$annual2 = number_format((float)$annual, 2, '.', '');
					$background = '#dcd9d9';
				}
			?>
				<div class="site-product-box-tick garung-product-box column-<?php echo $i; ?> khiem_column_<?php echo get_the_ID(); ?>" style="background-color: <?php echo $background; ?>; width: 20%; height: 92%; float: left; font-family: Arial, Helvetica, sans-serif; font-size: 9pt; color: #6c6060; padding: 0px 10px;">
					<div class="header-more" style="height: 130px; padding-top: 20px; text-align: center;">
						<div class="logo-lifelock-text">
							<div class="logo"><img src="<?php bloginfo('template_directory'); ?>/image/logo-icon-text.gif" alt="Life Lock promo code" width="37" height="36"></div>
							<div class="text">
								<p>Life<span>Lock</span></p>
								<p><?php the_title() ?> ™</p>
							</div>
						</div>
						<p style="padding-top: 10px;"><span style="color: #000000;">$<?php echo $month; ?>/month</span></p>
						<p><span style="color: #999;">$<?php echo $annual2;  ?>/annual</span></p>
						<!-- <p><span style="color: #999;"><?php echo $max_term; ?> DAYS RISK FREE*</span><span style="font-size: 9pt;">&nbsp;</span></p> -->
						<p>&nbsp;</p>
						<p><a href="<?php echo render_url($id_code, $id_page); ?>" class="btn" style="color: white;" title="Enroll now">Enroll now</a></p>
					</div>
					<div class="body-more" style="height: 270px;">
						<div class="body-more1" style="height: 85px;"></div>
						<div class="body-more2">
							<?php the_content() ?>
						</div>
					</div>
					<div class="footer-more" style="padding: 14px 5px;">
						<ul id="feature" class="detail-tick khiem_check_<?php echo get_the_ID(); ?>">
							<?php
                            
                            $checked = get_field('option', get_the_ID());
                            	for ($i = 0; $i <= 25; $i++) { 
                            		if (($i%2) == 0) {
                            			echo "
                            				<li style='height: 30px; padding: 5px; background-color: #f0ebeb;'>
	                                    		<span>&nbsp;</span>
	                                    	</li>
                            			";
                            		}else{
                            			if ($i == 9) {
                            				echo "
                            					<li style='padding: 5px; height: 30px;  line-height: 30px; font-size: 15px; font-weight: bold; text-align: center;' class='check_li_num'> ". ((get_field('stolen_funds_replacement', get_the_ID()) != '') ? '$ '.get_field('stolen_funds_replacement', get_the_ID()): '') ."</li>
                            				";
                            			}
                            			echo "
                            				<li style='padding: 5px; height: 30px;'><span></span></li>
                            			";
                            		}
                            	}
                            ?>
						</ul>
					</div>
				</div>
			<?php
				$i++;
			endwhile;
		?>
		</div>
		</div><!-- #content -->
	</div><!-- #primary -->
	<div class="box-shadow footer-bot-product" style="font-family: Arial, helvetica, sans-serif; margin:0 0px; font-size: 12px; color: #6c6060;">
		<?php //if ( is_active_sidebar( 'footer-bot-product' ) ) : ?>
		    <!-- <ul id="sidebar">
		        <?php //dynamic_sidebar( 'footer-bot-product' ); ?>
		    </ul> -->
		<?php //endif; ?>
		<div class="custom">
		    <p>
		        <span style="font-size: 10pt; font-family: helvetica;">The credit scores provided are VantageScore 3.0 credit scores based on data from Equifax, Experian and TransUnion respectively. Any one bureau VantageScore mentioned is based on Equifax data only. Third parties use many different types of credit scores and are likely to use a different type of credit score to assess your creditworthiness.</span>
		    </p>
		    <p>
		        <span style="font-size: 10pt; font-family: helvetica;">
		            1 Credit reports, scores and credit monitoring may require an additional verification process and credit services will be withheld until such process is complete.
		        </span>
		    </p>

		    <p>
		        <span style="font-size: 10pt; font-family: helvetica;">
		            2 For LifeLock Ultimate Plus™ three bureau credit monitoring, credit monitoring from Experian and TransUnion will take several days to begin.
		        </span>
		    </p>

		    <p>
		        <span style="font-size: 10pt; font-family: helvetica;">
		           *Important Pricing & Subscription Details:
		        </span>
		    </p>

		    <p>
		        <span style="font-size: 10pt; font-family: helvetica;">
		           ◦ By subscribing, you are purchasing a recurring membership that begins when your purchase is completed and will automatically renew after your first paid term.
		        </span>
		    </p>
		    <p>
		        <span style="font-size: 10pt; font-family: helvetica;">
		            ◦ The price quoted today may include an introductory offer. Depending on your selection, your membership will automatically renew and be billed at the applicable monthly or annual renewal price found here. The price is subject to change, but we will always notify you in advance. This offer not combinable with other offers.
		        </span>
		    </p>
		    <p>
		        <span style="font-size: 10pt; font-family: helvetica;">
		             ◦ The credit reports, scores, and credit monitoring features may require an additional verification process and credit services will be withheld until such process is complete.
		        </span>
		    </p>
		    <p>
		        <span style="font-size: 10pt; font-family: helvetica;">
		             ◦ You can cancel your membership at any time here, or by contacting Member Services at: 844-488-4540. If you are an annual member and request a refund within 60-days after being billed, you are entitled to a complete refund. Otherwise, you are eligible for a pro-rated refund on any unused months through the end of your term. For more details, please visit the LifeLock Refund Policy.
		        </span>
		    </p>

		    <p>
		        <span style="font-size: 10pt; font-family: helvetica;">
		            No one can prevent all identity theft.
		        </span>
		    </p>
		    <p>
		        <span style="font-size: 10pt; font-family: helvetica;">
		            † LifeLock does not monitor all transactions at all businesses.
		        </span>
		    </p>
		    <p>
		        <span style="font-size: 10pt; font-family: helvetica;">
		            ‡ Reimbursement and Expense Compensation, each with limits of up to $25,000 for Standard, up to $100,000 for Advantage and up to $1 million for Ultimate Plus. And up to $1 million for coverage for lawyers and experts if needed, for all plans. Benefits provided by Master Policy issued by United Specialty Insurance Company, Inc. (State National Insurance Company, Inc. for NY State members). Policy terms, conditions and exclusions at: <a href="//LifeLock.com/legal" title="LifeLock.com/legal" style="color: #0D5EA8;
		    text-decoration: underline;">LifeLock.com/legal</a>
		        </span>
		    </p>
		</div>
	</div>
<?php endwhile; // end of the loop. ?>
<script type="text/javascript">
	// jQuery(document).ready(function(){
	var window_width = jQuery (window).width();
	if(window_width < 480) {
		jQuery('.column-1').addClass('col_show');
		jQuery('.column-2').hide();
		jQuery('.column-3').hide();
		jQuery('.column-4').hide();

		jQuery('.garung_plan_feature').css('width', '60%');
		jQuery('.garung_tab_a').click(function(){
			var num = jQuery(this).attr('data-num');
			if (num == 1) {
				jQuery('.column-1').addClass('col_show').show();
				jQuery('.column-2').hide();
				jQuery('.column-3').hide();
				jQuery('.column-4').hide();
			} 
			if (num == 2) {
				jQuery('.column-2').addClass('col_show').show();
				jQuery('.column-1').hide();
				jQuery('.column-3').hide();
				jQuery('.column-4').hide();
			} 
			if (num == 3) {
				jQuery('.column-3').addClass('col_show').show();
				jQuery('.column-1').hide();
				jQuery('.column-2').hide();
				jQuery('.column-4').hide();
			} 
			if (num == 4) {
				jQuery('.column-4').addClass('col_show').show();
				jQuery('.column-1').hide();
				jQuery('.column-2').hide();
				jQuery('.column-3').hide();
			}
		});
	}
	// });
	function responsive_column (col_show) {
		switch (col_show) {
			case 1 : 
				jQuery('.column-1').addClass('col_show');
				jQuery('.column-2').hide();
				jQuery('.column-3').hide();
				jQuery('.column-4').hide();
				break;
			case 2: 
				jQuery('.column-2').addClass('col_show');
				jQuery('.column-1').hide();
				jQuery('.column-3').hide();
				jQuery('.column-4').hide();
				break;
			case 3: 
				jQuery('.column-3').addClass('col_show');
				jQuery('.column-1').hide();
				jQuery('.column-2').hide();
				jQuery('.column-4').hide();
				break;
			case 4: 
				jQuery('.column-4').addClass('col_show');
				jQuery('.column-1').hide();
				jQuery('.column-2').hide();
				jQuery('.column-3').hide();
				break;
			default : {
				jQuery('.column-1').addClass('col_show');
				jQuery('.column-2').addClass('tab_content_hide');
				jQuery('.column-3').hide();
				jQuery('.column-4').hide();
			}
		}
	}
</script>
<?php get_footer() ?>