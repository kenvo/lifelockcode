<?php
/*
Template Name: Pricing
*/
get_header(); ?>
<?php while ( have_posts() ) : the_post();
    $title = get_the_title();
    $i = 1;
    $arr_infoproduct = [];
    $current_post_id = get_the_ID();
    $info_products = new WP_Query( array( 'post_type' => 'info_products','posts_per_page'=> -1) ); 
    while($info_products->have_posts() ) : 
        $info_products->the_post();
        $arr_infoproduct[get_the_title()]['price_month'] = (float)get_field('price_month', get_the_ID());
        $arr_infoproduct[get_the_title()]['price_annual'] = (float)get_field('price_annual', get_the_ID());
        $arr_infoproduct[get_the_title()]['id'] = get_the_ID();
    endwhile;
    $loop = new WP_Query( array( 'post_type' => 'Code','posts_per_page'=> -1) );

    $tmp_loop = $loop;

   foreach ($tmp_loop->posts as $key => $post) {
        $order = get_field('order', $post->ID);
        $post->order = $order;
    }

    usort($tmp_loop->posts, function($a, $b) {
        return ($a->order <= $b->order);
    });

    $max_code_title = get_the_title($tmp_loop->posts[0]->ID);
    $max_code_discount = get_post_meta($tmp_loop->posts[0]->ID, 'discount', true);
    $max_code_term = get_post_meta($tmp_loop->posts[0]->ID, 'term', true);
    if(!$max_code_discount) {
        $max_code_discount = 0;
    }
    if(!$max_code_term) {
        $max_code_term = 0;
    }

?>

<div class="innerCont w1354 kenpricing" id="pricing-dropdown">
    <div class="largeTitle">
        <h1><?php echo $title; ?></h1>
    </div>
    <div class="box-shadow LifeLock-Pricing">
        <h2><span style="font-size: 1.17em;">LifeLock Pricing</span></h2>
        <div class="row">
            <div class="pricing-select col-md-7 col-sm-12">
                <h5>Select a Promo Code:</h5>
                <li class="dropdown aa">
                    <a href="#" data-toggle="dropdown" class="dropp-header__btn js-dropp-action custom-replace" title=""><span class="dropp-header__title js-value"><?php echo $max_code_title; ?> - Save <?php echo $max_code_discount;?>% off first year. All Plans* + Free Shredder<!--  + <?php echo $max_code_term; ?> Days Risk Free* --></span>
                        <span href="#" class="dropp-header__btn js-dropp-action" title="">
                            <i class="icon"></i>
                        </span>
                    </a>
                      
                        
                    
                    <ul class="dropdown-menu1 custom-list">
                        <?php $i=1; ?>
                        <?php while ( $loop->have_posts() ) : $loop->the_post();
                            // echo "<pre>";
                            // var_dump($post->order);
                            // die();

                            if ($post->order == 0 || $post->order == 2 || $post->order == 5)continue;{

                            } ?>
                        
                        <?php 
                            // if ($i > 1) {
                                $max_discount = get_post_meta(get_the_ID(), 'discount', true);
                                $max_term = get_post_meta(get_the_ID(), 'term', true);
                                if(!$max_discount) {
                                    $max_discount = 0;
                                }
                                if(!$max_term) {
                                    $max_term = 0;
                                }

                        ?>

                        <li value="llc<?php echo $i ?>" class="abc llc<?php echo $i ?> custom-list-<?php echo $i; ?>" data-title='<?php echo get_the_title(); ?>' data-url='<?php echo render_url(get_the_ID(), $current_post_id); ?>' max_discount='<?php echo $max_discount; ?>' max_term='<?php echo $max_term; ?>'><?php the_title(); ?>

                            <?php
                                // var_dump($post->order);
                                // if ($post->order == 1) {
                                //     the_field('promotion');
                                // }else{
                                //     if ($post->order == 6) {
                                //         echo "- Save 10% off first year. All Plans* + Free Shredder";
                                //     }else{
                                //         echo "- ";the_field('promotion');
                                //     }
                                // }

                                if ($post->order == 1) {
                                    the_field('promotion');
                                }elseif ($post->order == 6) {
                                    echo "- Save 10% off first year. All Plans* + Free Shredder";
                                }elseif ($post->order == 4) {
                                    echo "- Save 10% off first year. All Plans*";
                                }elseif ($post->order == 3) {
                                    echo "- Save 10% off first year. All Plans*";
                                }

                             ?>
                            
                        </li>

                        <?php ?>
                        <?php

                        // }
                        $i++;
                        endwhile; ?>

                    </ul>
                </li>
                 
                
                <p class="notification" style="margin-top: 20px; margin-bottom: -20px;font-size: 1.7em;">Prices Include Promo Code: <span style="font-size: 1.10em;" class="pri-title"></span></p>

            </div>

            <div class="logo_pricing col-md-5 col-sm-12">
                <div class="images">
                    <a href="http://lifelocktrk.com/?a=107&c=12&s1=dmn&s2=llpc&s3=LLC1030&s5=ll_logo_pricing">
                        <img class="img-responsive" src="<?php bloginfo('template_directory'); ?>/image/LL_Corp_H_4C.png">
                    </a>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-4 package_discount" data-package="Standard">
                <div class="ricing-title" style="min-height: auto;">
                    <div class="logo-lifelock-text"  >
                        <div class="logo"><img src="<?php bloginfo('template_directory'); ?>/image/logo-icon-text.gif" alt="price of LifeLock" width="37" height="36"></div>
                        <div class="text">
                            <p>Life<span>Lock</span></p>
                            <p>Standard ™</p>
                        </div>
                    </div>
                </div>
                <div class="container-fluid pricing-product">
                    <div class="row llc1 ll3015 box">
                        <div class="col-sm-12 bang-gia">
                            <div class="ricing-header" style="text-align: center; background-color: #737070;">
                                <div class="custom-price-1 pricing-lifelock"></div>
                                <a href="" class="pri-a" title="Enroll Button 1"><img src="<?php bloginfo('template_directory'); ?>/image/enroll-button-1.png" class="btn-img-hover" alt="Life Lock promo code"><br></a></div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-12 text_discription">
                            <div class="ricing-content">
                                <p>Million Dollar Protection™ Package‡</p>
                                <?php
                                    $colors = get_field('option',$arr_infoproduct['STANDARD']['id']);
                                    if( $colors ): ?>
                                        <?php foreach( $colors as $color ): ?>
                                            <p><?php echo $color; ?></p>
                                        <?php endforeach; ?>
                                    <?php endif; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-sm-4 package_discount" data-package="Advantage">
                <div class="ricing-title"  style="min-height: auto;">
                    <div class="logo-lifelock-text">
                        <div class="logo"><img src="<?php bloginfo('template_directory'); ?>/image/logo-icon-text.gif" alt="price of LifeLock" width="37" height="36"></div>
                        <div class="text">
                            <p>Life<span>Lock</span></p>
                            <p>Advantage ™</p>
                        </div>
                    </div>
                </div>
                
                <div class="container-fluid pricing-product">
                    <div class="row llc1 ll3015 box">
                        <div class="col-sm-12 bang-gia">
                            <div class="ricing-header" style="text-align: center; background-color: #737070;">
                                <div class="custom-price-2 pricing-lifelock"></div>
                            <a href="" class="pri-a" title="Enroll Button 1"><img src="<?php bloginfo('template_directory'); ?>/image/enroll-button-1.png" class="btn-img-hover" alt="Life Lock promo code"><br></a> </div>  
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-12 text_discription">
                            <div class="ricing-content advantage">
                                <p>Million Dollar Protection™ Package‡</p>
                                 <?php
                                    $colors = get_field('option',$arr_infoproduct['ADVANTAGE']['id']);
                                    if( $colors ): ?>
                                        <?php foreach( $colors as $color ): ?>
                                            <p><?php echo $color; ?></p>
                                        <?php endforeach; ?>
                                    <?php endif; ?>
                                <p class="text_hightline">
                                    Annual Credit Report & Score: <br>
                                    One Bureau<sup>1</sup> <br>
                                    <i>The credit score provided is a VantageScore 3.0 credit score based on Equifax data. Third parties use many different types of credit scores and are likely to use a different type of credit score to assess your creditworthiness.</i>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

            <div class="col-sm-4 package_discount" data-package="Ultimate">
                <div class="ricing-title"  style="min-height: auto;">
                    <div class="logo-lifelock-text">
                        <div class="logo"><img src="<?php bloginfo('template_directory'); ?>/image/logo-icon-text.gif" alt="price of LifeLock" width="37" height="36"></div>
                        <div class="text">
                            <p>Life<span>Lock</span></p>
                            <p>Ultimate plus ™</p>
                        </div>
                    </div>
                </div>

                <div class="container-fluid pricing-product">
                    <div class="row llc1 ll3015 box">
                        <div class="col-sm-12 bang-gia">
                            <div class="ricing-header" style="text-align: center; background-color: #737070;">
                            <div class="custom-price-3 pricing-lifelock"></div>
                            <a href="" class="pri-a" title="Enroll Button 1"><img src="<?php bloginfo('template_directory'); ?>/image/enroll-button-1.png" class="btn-img-hover" alt="Life Lock promo code"><br></a> </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-12 text_discription">
                            <div class="ricing-content ultimate_plus">
                                <p>Million Dollar Protection™ Package‡</p>
                                 <?php
                                $colors = get_field('option',$arr_infoproduct['ULTIMATE PLUS']['id']);

                                if( $colors ): ?>
                                    <?php foreach( $colors as $color ): ?>
                                        <p><?php echo $color; ?></p>
                                    <?php endforeach; ?>
                                <?php endif; ?>
                                <p class="text_hightline">
                                    Annual Credit Reports & Scores: <br>
                                    Three Bureaus<sup>1</sup> <br>
                                    <i>The credit scores provided are VantageScore 3.0 credit scores based on data from Equifax, Experian and TransUnion respectively. Third parties use many different types of credit scores and are likely to use a different type of credit score to assess your creditworthiness.</i>
                                </p>

                                <p class="text_hightline">
                                    Monthly Credit Score Tracking: <br>
                                    One Bureau<sup>1</sup> <br>
                                    <i>
                                        The credit score provided is a VantageScore 3.0 credit score based on Equifax data. Third parties use many different types of credit scores and are likely to use a different type of credit score to assess your creditworthiness.
                                    </i>
                                </p>
                                <p>
                                    401(k) & Investment Activity Alerts†
                                </p>
                                <p>
                                    Bank Account Takeover & New Account Alerts†
                                </p>
                                <p>
                                    File-Sharing Network Searches
                                </p>
                                <p>
                                    Sex Offender Registry Reports
                                </p>
                                <p>
                                    Priority Live Member Support
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>   
        <!-- </div> -->

            <script type="text/javascript" src="https://code.jquery.com/jquery.min.js"></script>
<script type="text/javascript">
$(document).ready(function(){
    $('.custom-list').hide();
    detail_code('.custom-list-1');
    $(".abc").click(function(){
            
            detail_code(this);
    });    

    $( 'a[data-toggle="dropdown"]' ).click( function() {
        $( 'ul.dropdown-menu1' ).slideToggle();
    } );

    $( document ).click( function(e) {
        if( e.target !== document.querySelectorAll( 'a[data-toggle="dropdown"]' )[0] && e.target !== document.querySelectorAll( 'a[data-toggle="dropdown"] .dropp-header__btn' )[0] && e.target !== document.querySelectorAll( 'a[data-toggle="dropdown"] i' )[0] && e.target !== document.querySelectorAll( 'a[data-toggle="dropdown"] .dropp-header__title' )[0]) {
            $( 'ul.dropdown-menu1' ).hide();     
        }
    } );

    $('.abc').click(function(){
        var content = $(this).text();
        console.log(content);
        $('.custom-replace').html('<span class="dropp-header__title js-value">'+content+'</span><span href="#" class="dropp-header__btn js-dropp-action" title=""><i class="icon"></i></span>');
    });

});
function detail_code(tag) {
    var title = $(tag).attr('data-title');
    var return_url = $(tag).attr('data-url');
    var max_discount = $(tag).attr('max_discount');
    var max_term = $(tag).attr('max_term');
    var str_show = '';

    if (title === 'LLCSHRED') {
        if (max_discount == 0 && max_term == 0) {
            $('.notification').html('');
            str_show = 'Prices Include No Promo Code';
        }else{
            if (max_discount == 0 && max_term == 30 || max_term == 60 ) {
                $('.notification').html('Prices Include Promo Code: <span style="font-size: 1.05em;" class="pri-title"></span>');
                str_show = '<a href="'+return_url+'" style="corlor: #333;" title="'+title+'">'+title+'</a> ('+max_term+' Days Risk Free*) <span style="font-size: 0.8em;">*Terms Apply</span>';
            }else{
                $('.notification').html('Prices Include Promo Code: <span style="font-size: 1.05em;" class="pri-title"></span>');
                str_show = '<a href="'+return_url+'" style="corlor: #333;" title="'+title+'">'+title+'</a> (Save 10% off first year*) <span style="font-size: 0.8em;">*Terms Apply</span>';
            }
            
        }

        var standard_month = <?php echo (!empty($arr_infoproduct['STANDARD']['price_month']) ? $arr_infoproduct['STANDARD']['price_month'] : 0); ?>;
        var standard_annual = <?php echo (!empty($arr_infoproduct['STANDARD']['price_annual']) ? $arr_infoproduct['STANDARD']['price_annual'] : 0); ?>;

        var junior_month = <?php echo (!empty($arr_infoproduct['JUNIOR']['price_month']) ? $arr_infoproduct['JUNIOR']['price_month'] : 0); ?>;
        var junior_annual = <?php echo (!empty($arr_infoproduct['JUNIOR']['price_annual']) ? $arr_infoproduct['JUNIOR']['price_annual'] : 0); ?>;

        var ultimate_month = <?php echo (!empty($arr_infoproduct['ULTIMATE PLUS']['price_month']) ? $arr_infoproduct['ULTIMATE PLUS']['price_month'] : 0); ?>;
        var ultimate_annual = <?php echo (!empty($arr_infoproduct['ULTIMATE PLUS']['price_annual']) ? $arr_infoproduct['ULTIMATE PLUS']['price_annual'] : 0); ?>;

        var junior_month = <?php echo (!empty($arr_infoproduct['ADVANTAGE']['price_month']) ? $arr_infoproduct['ADVANTAGE']['price_month'] : 0); ?>;
        var junior_annual = <?php echo (!empty($arr_infoproduct['ADVANTAGE']['price_annual']) ? $arr_infoproduct['ADVANTAGE']['price_annual'] : 0); ?>;

        // calulating price and rounding price

        var price_standard_month = standard_month * ((100 - (parseFloat(max_discount)))/100);
        var price_standard_annual = standard_annual * ((100 - (parseFloat(max_discount)))/100);

        var price_junior_month = junior_month * ((100 - 10)/100);
        var price_junior_annual = junior_annual * ((100 - 10)/100);

        var price_ultimate_month = ultimate_month * ((100 - 10)/100);
        var price_ultimate_annual = ultimate_annual * ((100 - 10)/100);

        if(!price_standard_month) {
            price_standard_month = 0;
        }
        if(!price_standard_annual) {
            price_standard_annual = 0;
        }
        if(!price_junior_month) {
            price_junior_month = 0;
        }
        if(!price_junior_annual) {
            price_junior_annual = 0;
        }
        if(!price_ultimate_month) {
            price_ultimate_month = 0;
        }
        if(!price_ultimate_annual) {
            price_ultimate_annual = 0;
        }
            price_standard_month = price_standard_month.toFixed(2);
            price_standard_annual = price_standard_annual.toFixed(2);

            price_junior_month = price_junior_month.toFixed(2);
            price_junior_annual = price_junior_annual.toFixed(2);

            console.log(price_junior_month, price_junior_annual);

            price_ultimate_month = price_ultimate_month.toFixed(2);
            price_ultimate_annual = price_ultimate_annual.toFixed(2);

        $('.custom-price-1').html('<h3 style="color: #fff; margin-bottom: 0px; " class="month1">$'+price_standard_month+'/month</h3><h3 style="color: #fff;padding-top:10px;" class="annual1">$'+price_standard_annual+'/annual</h3>');
        $('.custom-price-2').html('<h3 style="color: #fff; margin-bottom: 0px;" class="month1">$'+price_junior_month+'/month</h3><h3 style="color: #fff;padding-top:10px;" class="annual1">$'+price_junior_annual+'/annual</h3>');

        $('.custom-price-3').html('<h3 style="color: #fff; margin-bottom: 0px;" class="month1">$'+price_ultimate_month+'/month</h3><h3 style="color: #fff;padding-top:10px;" class="annual1">$'+price_ultimate_annual+'/annual</h3>');

        $('.pri-a').attr('href', return_url);
        if (max_discount == 0 && max_term == 0) {
            $('.notification').html(str_show);
        }else{

            $('.pri-title').html(str_show);
        }     
    }else{
        if (max_discount == 0 && max_term == 0) {
            $('.notification').html('');
            str_show = 'Prices Include No Promo Code';
        }else{
            if (max_discount == 0 && max_term == 30 || max_term == 60 ) {
                $('.notification').html('Prices Include Promo Code: <span style="font-size: 1.05em;" class="pri-title"></span>');
            str_show = '<a href="'+return_url+'" style="corlor: #333;" title="'+title+'">'+title+'</a> ('+max_term+' Days Risk Free*)';
            }else{
                if (max_term == 0 && max_discount >= 0) {
                    $('.notification').html('Prices Include Promo Code: <span style="font-size: 1.05em;" class="pri-title"></span>');
            str_show = '<a href="'+return_url+'" style="corlor: #333;" title="'+title+'">'+title+'</a> (Save 10% off first year*) <span style="font-size: 0.8em;">*Terms Apply</span>';
                }else{
                    $('.notification').html('Prices Include Promo Code: <span style="font-size: 1.05em;" class="pri-title"></span>');
            str_show = '<a href="'+return_url+'" style="corlor: #333;" title="'+title+'">'+title+'</a> (Save 10% off first year*) <span style="font-size: 0.8em;">*Terms Apply</span>';
                }
                
            }
            
        }

        var standard_month = <?php echo (!empty($arr_infoproduct['STANDARD']['price_month']) ? $arr_infoproduct['STANDARD']['price_month'] : 0); ?>;
        var standard_annual = <?php echo (!empty($arr_infoproduct['STANDARD']['price_annual']) ? $arr_infoproduct['STANDARD']['price_annual'] : 0); ?>;

        var junior_month = <?php echo (!empty($arr_infoproduct['JUNIOR']['price_month']) ? $arr_infoproduct['JUNIOR']['price_month'] : 0); ?>;
        var junior_annual = <?php echo (!empty($arr_infoproduct['JUNIOR']['price_annual']) ? $arr_infoproduct['JUNIOR']['price_annual'] : 0); ?>;

        var ultimate_month = <?php echo (!empty($arr_infoproduct['ULTIMATE PLUS']['price_month']) ? $arr_infoproduct['ULTIMATE PLUS']['price_month'] : 0); ?>;
        var ultimate_annual = <?php echo (!empty($arr_infoproduct['ULTIMATE PLUS']['price_annual']) ? $arr_infoproduct['ULTIMATE PLUS']['price_annual'] : 0); ?>;

        var junior_month = <?php echo (!empty($arr_infoproduct['ADVANTAGE']['price_month']) ? $arr_infoproduct['ADVANTAGE']['price_month'] : 0); ?>;
        var junior_annual = <?php echo (!empty($arr_infoproduct['ADVANTAGE']['price_annual']) ? $arr_infoproduct['ADVANTAGE']['price_annual'] : 0); ?>;

        // calulating price and rounding price 
        var price_standard_month = standard_month * ((100 - (parseFloat(max_discount)))/100);
        var price_standard_annual = standard_annual * ((100 - (parseFloat(max_discount)))/100);

        var price_junior_month = junior_month * ((100 - (parseFloat(max_discount)))/100);
        var price_junior_annual = junior_annual * ((100 - (parseFloat(max_discount)))/100);

        var price_ultimate_month = ultimate_month * ((100 - (parseFloat(max_discount)))/100);
        var price_ultimate_annual = ultimate_annual * ((100 - (parseFloat(max_discount)))/100);

        var price_ultimate_month = ultimate_month * ((100 - (parseFloat(max_discount)))/100);
        var price_ultimate_annual = ultimate_annual * ((100 - (parseFloat(max_discount)))/100);

        if(!price_standard_month) {
            price_standard_month = 0;
        }
        if(!price_standard_annual) {
            price_standard_annual = 0;
        }
        if(!price_junior_month) {
            price_junior_month = 0;
        }
        if(!price_junior_annual) {
            price_junior_annual = 0;
        }
        if(!price_ultimate_month) {
            price_ultimate_month = 0;
        }
        if(!price_ultimate_annual) {
            price_ultimate_annual = 0;
        }
            price_standard_month = price_standard_month.toFixed(2);
            price_standard_annual = price_standard_annual.toFixed(2);

            price_junior_month = price_junior_month.toFixed(2);
            price_junior_annual = price_junior_annual.toFixed(2);

            price_ultimate_month = price_ultimate_month.toFixed(2);
            price_ultimate_annual = price_ultimate_annual.toFixed(2);

        $('.custom-price-1').html('<h3 style="color: #fff; margin-bottom: 0px; " class="month1">$'+price_standard_month+'/month</h3><h3 style="color: #fff;padding-top:10px;" class="annual1">$'+price_standard_annual+'/annual</h3>');
        $('.custom-price-2').html('<h3 style="color: #fff; margin-bottom: 0px;" class="month1">$'+price_junior_month+'/month</h3><h3 style="color: #fff;padding-top:10px;" class="annual1">$'+price_junior_annual+'/annual</h3>');

        $('.custom-price-3').html('<h3 style="color: #fff; margin-bottom: 0px;" class="month1">$'+price_ultimate_month+'/month</h3><h3 style="color: #fff;padding-top:10px;" class="annual1">$'+price_ultimate_annual+'/annual</h3>');

        $('.pri-a').attr('href', return_url);
        if (max_discount == 0 && max_term == 0) {
            $('.notification').html(str_show);
        }else{

            $('.pri-title').html(str_show);
        }
    }
}
</script>
            <?php endwhile; // end of the loop. ?>
            </div><!-- #content -->
            <div class="box-shadow footer-bot-product" style="font-family: Arial, helvetica, sans-serif; margin:45px 0px; font-size: 12px; color: #6c6060;">
                <?php //if ( is_active_sidebar( 'footer-bot-product' ) ) : ?>
                    <!-- <ul id="sidebar">
                        <?php //dynamic_sidebar( 'footer-bot-product' ); ?>
                    </ul> -->
                <?php //endif; ?>
                <div class="custom">
                    <p>
                        <span style="font-size: 10pt; font-family: helvetica;">The credit scores provided are VantageScore 3.0 credit scores based on data from Equifax, Experian and TransUnion respectively. Any one bureau VantageScore mentioned is based on Equifax data only. Third parties use many different types of credit scores and are likely to use a different type of credit score to assess your creditworthiness.</span>
                    </p>
                    <p>
                        <span style="font-size: 10pt; font-family: helvetica;">
                            1 Credit reports, scores and credit monitoring may require an additional verification process and credit services will be withheld until such process is complete.
                        </span>
                    </p>

                    <p>
                        <span style="font-size: 10pt; font-family: helvetica;">
                            2 For LifeLock Ultimate Plus™ three bureau credit monitoring, credit monitoring from Experian and TransUnion will take several days to begin.
                        </span>
                    </p>

                    <p>
                        <span style="font-size: 10pt; font-family: helvetica;">
                           *Important Pricing & Subscription Details:
                        </span>
                    </p>

                    <p>
                        <span style="font-size: 10pt; font-family: helvetica;">
                           ◦ By subscribing, you are purchasing a recurring membership that begins when your purchase is completed and will automatically renew after your first paid term.
                        </span>
                    </p>
                    <p>
                        <span style="font-size: 10pt; font-family: helvetica;">
                            ◦ The price quoted today may include an introductory offer. Depending on your selection, your membership will automatically renew and be billed at the applicable monthly or annual renewal price found here. The price is subject to change, but we will always notify you in advance. This offer not combinable with other offers.
                        </span>
                    </p>
                    <p>
                        <span style="font-size: 10pt; font-family: helvetica;">
                             ◦ The credit reports, scores, and credit monitoring features may require an additional verification process and credit services will be withheld until such process is complete.
                        </span>
                    </p>
                    <p>
                        <span style="font-size: 10pt; font-family: helvetica;">
                             ◦ You can cancel your membership at any time here, or by contacting Member Services at: 844-488-4540. If you are an annual member and request a refund within 60-days after being billed, you are entitled to a complete refund. Otherwise, you are eligible for a pro-rated refund on any unused months through the end of your term. For more details, please visit the LifeLock Refund Policy.
                        </span>
                    </p>

                    <p>
                        <span style="font-size: 10pt; font-family: helvetica;">
                            No one can prevent all identity theft.
                        </span>
                    </p>
                    <p>
                        <span style="font-size: 10pt; font-family: helvetica;">
                            † LifeLock does not monitor all transactions at all businesses.
                        </span>
                    </p>
                    <p>
                        <span style="font-size: 10pt; font-family: helvetica;">
                            ‡ Reimbursement and Expense Compensation, each with limits of up to $25,000 for Standard, up to $100,000 for Advantage and up to $1 million for Ultimate Plus. And up to $1 million for coverage for lawyers and experts if needed, for all plans. Benefits provided by Master Policy issued by United Specialty Insurance Company, Inc. (State National Insurance Company, Inc. for NY State members). Policy terms, conditions and exclusions at: <a href="//LifeLock.com/legal" title="LifeLock.com/legal" style="color: #0D5EA8;
                    text-decoration: underline;">LifeLock.com/legal</a>
                        </span>
                    </p>
                </div>
            </div>
            </div><!-- #primary -->
            <?php get_footer() ?>