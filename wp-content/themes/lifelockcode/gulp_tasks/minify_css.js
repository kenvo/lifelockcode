var gulp = require('gulp');
var cssmin = require('gulp-cssmin');
var rename = require('gulp-rename');
var concat = require("gulp-concat");

module.exports = [
    ['sass'],
    function() {
        gulp.src([
                './bower_components/bootstrap/dist/css/bootstrap.css',
                './bower_components/bootstrap/dist/css/bootstrap-theme.css',
                './bower_components/font-awesome/css/font-awesome.css',
                './dist/css/app.css',
                './assets/css/**/*.css'
            ])
            .pipe(cssmin())
            .pipe(rename({
                suffix: '.min'
            }))
            .pipe(concat("app.min.css"))
            .pipe(gulp.dest('./dist/css/'));
    }
];
