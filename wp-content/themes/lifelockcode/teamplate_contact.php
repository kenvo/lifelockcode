<?php 
/*
 Template Name: Contact
*/
 get_header(); ?>
	
	<?php while ( have_posts() ) : the_post(); ?>

			<?php //the_field('home-section1'); ?>

			
			<div class="innerCont w1354">
			<div class="largeTitle">
				<h1><?php the_title(); ?></h1>

			</div>
			<div class="box-shadow shadow">

<div class="col-md-8 google-map">
<h3 style="padding-bottom: 20px;">Contact Info</h3>
<?php the_field('location') ?>
</div>
<div class="col-md-4 contact-right" style='font-family: "Open Sans";'>
<?php the_content(); ?>
</div>
</div>
</div>
	<?php endwhile; // end of the loop. ?>

	</div><!-- #content -->
</div><!-- #primary -->

 <?php get_footer() ?>