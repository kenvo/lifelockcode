
<?php
/*
Template Name: Reviews
 */

use Symfony\Component\DomCrawler\Crawler;
require_once __DIR__ . '/vendor/autoload.php';
?>
<?php
$get_url = str_replace('', '', $_SERVER['REQUEST_URI']);
$get_url = explode("?", $get_url);
$begin_get_var = 0;
$url_child = $get_url[$begin_get_var + 1];
$url_main = 'https://www.lifelock.com/reviews/' . $url_child;

$html = file_get_contents('https://www.lifelock.com/reviews/');
$crawler = new Crawler($html);

// echo "<pre>";
// var_dump($url_child);
// die();

try {
  $starsReviews = $crawler->filter('article.stats > section.col-sm-5.hidden-xs.stars')->html();
} catch (Exception $e) {}


// get content page 7-06-2018

$tab = $_GET['tab'];

$getData = '';
$getComment = '';
$rating = '';

if ($_GET['rating'] == null) {
  $rating = '';
}elseif ($_GET['rating'] == '5-star') {
  $rating = 5;
}elseif ($_GET['rating'] == '4-star') {
  $rating = 4;
}elseif ($_GET['rating'] == '3-star') {
  $rating = 3;
}elseif ($_GET['rating'] == '2-star') {
  $rating = 2;
}elseif ($_GET['rating'] == '1-star') {
  $rating = 1;
}
// $getComment = json_decode(file_get_contents('https://www.lifelock.com/bin/norton/lifelock/zuberance/reviews?includeMetrics=false&productId=&max=10'));

if ($tab == null) {
  $getData = json_decode(file_get_contents('https://www.lifelock.com/bin/norton/lifelock/zuberance/reviews?includeMetrics=true&productId=&rating='));
  $getComment = json_decode(file_get_contents('https://www.lifelock.com/bin/norton/lifelock/zuberance/reviews?includeMetrics=false&productId=&max=10&rating='. $rating .''));
}elseif ($tab == 'lifelock-standard') {
  $getData = json_decode(file_get_contents('https://www.lifelock.com/bin/norton/lifelock/zuberance/reviews?includeMetrics=true&productId=85323974&rating='));
  $getComment = json_decode(file_get_contents('https://www.lifelock.com/bin/norton/lifelock/zuberance/reviews?includeMetrics=false&productId=85323974&max=10&rating='. $rating .''));
}elseif ($tab == 'lifelock-advantage') {
  $getData = json_decode(file_get_contents('https://www.lifelock.com/bin/norton/lifelock/zuberance/reviews?includeMetrics=true&productId=92533817&rating='));
  $getComment = json_decode(file_get_contents('https://www.lifelock.com/bin/norton/lifelock/zuberance/reviews?includeMetrics=false&productId=92533817&max=10&rating='. $rating .''));
}elseif ($tab == 'lifelock-ultimate-plus') {
  $getData = json_decode(file_get_contents('https://www.lifelock.com/bin/norton/lifelock/zuberance/reviews?includeMetrics=true&productId=92533847&rating='));
  $getComment = json_decode(file_get_contents('https://www.lifelock.com/bin/norton/lifelock/zuberance/reviews?includeMetrics=false&productId=92533847&max=10&rating='. $rating .''));
}
$totalReviews_new = $getData->totalReviews;
$avgReviews_new = $getData->averageRating;
  
if (has_post_thumbnail()) {
  $image = wp_get_attachment_image_src(get_post_thumbnail_id(), 'full');
  $url = $image[0];
} else {
  $url = '';
}

get_header();
?>
<style type="text/css">
  #page-reviews .cover-img {
  background-image: url(<?php echo $url; ?>);
  background-size: 100% 100%;
  height: 530px;
  margin: 0 auto;
  width: 1200px;
}
</style>
<div id="page-reviews" class="box-shadow">
  <div id="data_all" style="display: none;" data-total="<?php echo $totalReviews_new; ?>" data-5="<?php echo $getData->ratings[0]->reviews ?>" data-4="<?php echo $getData->ratings[1]->reviews ?>" data-3="<?php echo $getData->ratings[2]->reviews ?>" data-2="<?php echo $getData->ratings[3]->reviews ?>" data-1="<?php echo $getData->ratings[4]->reviews ?>"></div>
  <?php while (have_posts()): the_post();?>
        <?php

          $title_review = get_the_title();
          $id_page = get_the_ID();
          $id_code = '134';
          $get_post_code = new WP_Query( array( 'post_type' => 'Code','posts_per_page'=> 1,) );
          while ( $get_post_code->have_posts() ) : $get_post_code->the_post();
            $id_code = get_the_ID();
          endwhile;

          $arr_infoproduct = [];
          $info_products = new WP_Query( array( 'post_type' => 'info_products','posts_per_page'=> -1,) ); 
            while($info_products->have_posts() ) : 
                $info_products->the_post();
                $arr_infoproduct[get_the_title()]['id'] = get_the_ID();
                $arr_infoproduct[get_the_title()]['option'] = get_field('option', get_the_ID());
            endwhile;
          $max_discount = get_post_meta($id_code, 'discount', true);
          $max_term = get_post_meta($id_code, 'term', true);
          $name_code = get_post_meta($id_code, 'code', true);

        ?>
			    <div class="cover-img">
			        <div class="cover-text">
			            <h2><?php echo $title_review;?></h2>
                  
                  <p class="title_mota" style="margin-bottom: -15px;font-size: 22px;" class="mota_title">You could miss certain identity threats by just monitoring your credit.  LifeLock see more, like if your personal information is sold on the dark web. And if there’s a problem, we’ll work to fix it</p>

			            <p><?php the_content(); ?></p>
			            <a href="<?php echo render_url($id_code, $id_page); ?>" target="_blank" title="<?php echo $name_code; ?> Save <?php echo $max_discount; ?>% + <?php echo $max_term; ?> Days Risk Free*">Promo Code: <?php echo $name_code; ?> | <!-- Save <?php the_field('code',$id_code); ?> --> <?php echo $max_discount; ?>% <!-- <?php echo $max_term; ?> Days Risk Free* -->off first year* + Free Shredder | ENROLL NOW</a>
                  <p style="    margin-bottom: 0px;text-align: left;margin-left: 120px; margin-top: 10px;font-style: italic;font-size: 14px;">*Terms Apply</p>
			            <div class="icon-togger">
			                <div class="point">   
                      </div>
			            </div>
			        </div>
			    </div>
			    <div class="content-reviews">
			        <!-- <p class="description" style="">
                <span>LifeLock helps to protect your identity through detection, alerts and restoration. There are 3 levels of protection starting at $9.99 per month (before <a title="promo code" href="<?php echo render_url($id_code, $id_page); ?>">PROMO CODE</a> savings). Signing up is an easy online process that is hassle-free*! The online wizard makes the process of enrolling simple. Once your registration is completed, LifeLock protection beings immediately. LifeLock monitors over a trillion data points daily. You will be alerted if suspicious activity has been realized. If your identity is compromised as a member of LifeLock, there will be a dedicated team of U.S. based specialists to assist with calls, paperwork filing and take additional steps in restoring your identity.</span><br><br>
                <span>Are you still uncertain and looking for more proof that LifeLock is the best identity theft-monitoring program? Read LifeLock reviews from members or unrelated companies and see what they have to say. The reviews consistently show how satisfied members are with their identity theft protection. When you are ready to be protected use Promo Code <a title="<?php echo $name_code; ?>" href="<?php echo render_url($id_code, $id_page); ?>"><?php echo $name_code; ?></a> for <?php echo $max_discount; ?>% off and <?php echo $max_term; ?> Days Risk Free* - <a title="Enroll Now!" href="<?php echo render_url($id_code, $id_page); ?>">ENROLL NOW!</a></span>
              </p> -->

              <p>While no one can prevent all identity theft, LifeLock helps protect your identity with three
layers of protection.</p>

              <p>First, they scan millions of transactions per second looking for potential threats to their
members’ personal information, including suspicious uses of name, address, phone number,
birth date and Social Security number, and a lot more.</p>

              <p>Second, even though no one can monitor all transactions at all businesses, the patented
LifeLock Identity Alert® system lets you know if they detect suspicious activity in their network.</p>

              <p>And third, if your identity does get stolen, they’ll assign a U.S.-Based Identity Restoration
Specialist to handle your case from start to finish and help fix the problem.</p>
			        <div class="client-tabs ">
			            <ul>
			                <li ><a class="" href="#tabs-1" title="Member Reviews">Member Reviews</a></li>
			                <!-- <li ><a href="#tabs-2" title="Website Reviews" class="active">Website Reviews</a></li> -->
			            </ul>
			        </div>
			        <div id="tabs-1" class="client-readmore tab-center" style="display: none;">
			            <div class="control-box-content">
			                <div class="control-the-star">
			                    <div class="title">
			                        <ul class="parent">
			                            <li class="title-child">
			                                <span>View</span>
			                                <ul>
			                                    <li><a href="<?php echo site_url(); ?>/reviews/" title="All Star Rating">All Star Rating</a></li>
			                                    <li><a href="<?php echo site_url(); ?>/reviews?tab=<?php echo $tab; ?>&rating=5-star" title="5 Star Rating">5 Star Rating</a></li>
			                                    <li><a href="<?php echo site_url(); ?>/reviews?tab=<?php echo $tab; ?>&rating=4-star" title="4 Star Rating">4 Star Rating</a></li>
			                                    <li><a href="<?php echo site_url(); ?>/reviews?tab=<?php echo $tab; ?>&rating=3-star" title="3 Star Rating">3 Star Rating</a></li>
			                                    <li><a href="<?php echo site_url(); ?>/reviews?tab=<?php echo $tab; ?>&rating=2-star" title="2 Star Rating">2 Star Rating</a></li>
			                                    <li><a href="<?php echo site_url(); ?>/reviews?tab=<?php echo $tab; ?>&rating=1-star" title="1 Star Rating">1 Star Rating</a></li>
			                                </ul>
			                            </li>
			                        </ul>
			                    </div>
			                </div>
			                <ul class="control-the-content">
			                    <li><a class="<?php echo $tab == null ? 'active' : ''; ?>" href="<?php echo site_url(); ?>/reviews/" title="All">All</a></li>
			                    <li><a class="<?php echo $tab == 'lifelock-standard' ? 'active' : ''; ?>" href="<?php echo site_url(); ?>/reviews?tab=lifelock-standard" title="Lifelock Standard">Lifelock Standard</a></li>
			                    <li><a class="<?php echo $tab == 'lifelock-advantage' ? 'active' : ''; ?>" href="<?php echo site_url(); ?>/reviews?tab=lifelock-advantage" title="Lifelock Advantage">Lifelock Advantage</a></li>
			                    <li><a class="<?php echo $tab == 'lifelock-ultimate-plus' ? 'active' : ''; ?>" href="<?php echo site_url(); ?>/reviews?tab=lifelock-ultimate-plus" title="Lifelock Ultimate Plus">Lifelock Ultimate Plus</a></li>
			                </ul>
			            </div>
			            <div class="box-reviews-star">
			                <div class="total-review">
			                    <h2><?php echo $totalReviews_new; ?></h2>
			                    <h3>REVIEWS</h3>
			                </div>
			                <div class="average-review">
			                    <h2><?php echo $avgReviews_new; ?></h2>
			                    <h3>AVERAGE REVIEWS</h3>
			                </div>
			                <div class="chart-review">
			                    <?php echo $starsReviews; ?>
			                </div>
			            </div>
			            <p class="mind-10-reviews"><span>*</span>Showing the most recent 10 reviews</p>
                  <div class="review_khiem"> 
                      <section class="prdReviews control-the-content">
                          <div class="tabs" id="tab-all">
                              <ul class="generalBoxWrap review-list">
                                  <?php foreach ($getComment->reviews->review as $value): ?>
                                    <li class="generalBox singleStory">
                                        <div class="starRating"><span class="span-<?php echo $rating; ?>"></span></div>
                                        <div class="generalBoxRight">
                                            <h2 class="reviewListTitle"><a href="<?php echo $value->reviewDetailLink; ?>" target="_blank"><?php echo $value->title ?></a></h2>
                                            <p class="description"><?php echo $value->description ?> <a href="<?php echo $value->reviewDetailLink; ?>" target="_blank" onclick="window.open('https://lifelock.zuberance.com/reviewV2/detail/180238187', 'detail', 'toolbar=0,status=0,scrollbars=1,width=944,height=680'); return false;" rel="nofollow">Read more »</a></p>
                                            <p class="reviewDetail">By <?php echo $value->screenName ?>, on <span><?php echo $value->reviewDate ?></span></p>
                                        </div>
                                        <div class="clear"></div>
                                  </li>
                                  <?php endforeach ?>
                              </ul>
                          </div>
                      </section>
                  </div>
			        </div>
			        <div id="tabs-2" class="client-readmore tab-center" style="display: block;">
			          <!-- ==================== use Jssor js ============================== -->
			            <div id="jssor_1" style="display: none; position: relative; margin: 0px auto; top: 0px; left: 0px; width: 809px; height: 82.551px; overflow: hidden; visibility: visible;" jssor-slider="true">

			            <div style="position: absolute; top: 0px; left: 0px; width: 980px; height: 100px; transform-origin: 0px 0px 0px; transform: scale(0.82551);"><div class="" style="display: block; position: relative; margin: 0px auto; top: 0px; left: 0px; width: 980px; height: 100px; overflow: visible; visibility: visible;"><div data-u="slides" style="cursor: default; position: absolute; top: 0px; left: 0px; width: 980px; height: 100px; overflow: hidden; z-index: 0;"><div style="position: absolute; z-index: 0; pointer-events: none; left: 0px; top: 0px;"></div></div><div data-u="slides" style="cursor: default; position: absolute; top: 0px; left: 0px; width: 980px; height: 100px; overflow: hidden; z-index: 0;"><div style="width: 140px; height: 100px; top: 0px; left: 0px; position: absolute; background-color: rgb(0, 0, 0); opacity: 0;"></div>
			                </div></div></div></div>
			            <!-- ===================================================================== -->
			            <ul>
                    <li class="client">
                        <div class="client-comment">
                            <h4>PCMAG</h4>
                            <p class="previews-calendar">
                                <span><img src="../templates/requestlifelocktheme/images/page_template/client-vote.jpg"></span>
                                <span><i class="fa fa-calendar"></i> December 19, 2015</span>
                            </p>
                            <p class="description comment read-more-text">
                                <a href="http://www.pcmag.com/article2/0%2c2817%2c2478669%2c00.asp"  target="_blank" >http://www.pcmag.com/article2/0%2c2817%2c2478669%2c00.asp</a>
                            </p>
                        </div>
                    </li>
                    <li class="client">
                        <div class="client-comment">
                            <h4>Tom’s Guide</h4>
                            <p class="previews-calendar">
                                <span><img src="../templates/requestlifelocktheme/images/page_template/client-vote.jpg"></span>
                                <span><i class="fa fa-calendar"></i> December 19, 2015</span>
                            </p>
                            <p class="description comment read-more-text">
                                <a href="http://www.tomsguide.com/us/lifelock-ultimate-plus,review-2807.html"  target="_blank" >http://www.tomsguide.com/us/lifelock-ultimate-plus,review-2807.html</a>
                            </p>
                        </div>
                    </li>
                    <li class="client">
                        <div class="client-comment">
                            <h4>Next Advisor</h4>
                            <p class="previews-calendar">
                                <span><img src="../templates/requestlifelocktheme/images/page_template/client-vote.jpg"></span>
                                <span><i class="fa fa-calendar"></i> December 19, 2015</span>
                            </p>
                            <p class="description comment read-more-text">
                                <a href="http://www.nextadvisor.com/identity_theft_protection_services/lifelock_ultimate_plus_review.php"  target="_blank" >http://www.nextadvisor.com/identity_theft_protection_services/lifelock_ultimate_plus_review.php</a>
                            </p>
                        </div>
                    </li>
                    <li class="client">
                        <div class="client-comment">
                            <h4>Theft Authority</h4>
                            <p class="previews-calendar">
                                <span><img src="../templates/requestlifelocktheme/images/page_template/client-vote.jpg"></span>
                                <span><i class="fa fa-calendar"></i> December 19, 2015</span>
                            </p>
                            <p class="description comment read-more-text">
                                <a href="http://www.idtheftauthority.com/reviews/lifelock/"  target="_blank" >http://www.idtheftauthority.com/reviews/lifelock/</a>
                            </p>
                        </div>
                    </li>
                    <li class="client">
                        <div class="client-comment">
                            <h4>Elite Personal Finance</h4>
                            <p class="previews-calendar">
                                <span><img src="../templates/requestlifelocktheme/images/page_template/client-vote.jpg"></span>
                                <span><i class="fa fa-calendar"></i> December 19, 2015</span>
                            </p>
                            <p class="description comment read-more-text">
                                <a href="http://www.elitepersonalfinance.com/lifelock/"  target="_blank" >http://www.elitepersonalfinance.com/lifelock/</a>
                            </p>
                        </div>
                    </li>
                    <li class="client">
                        <div class="client-comment">
                            <h4>Linkedin</h4>
                            <p class="previews-calendar">
                                <span><img src="../templates/requestlifelocktheme/images/page_template/client-vote.jpg"></span>
                                <span><i class="fa fa-calendar"></i> December 19, 2015</span>
                            </p>
                            <p class="description comment read-more-text">
                                <a href="https://www.linkedin.com/pulse/lifelock-reviews-news-report-now-published-quinn-william"  target="_blank" >https://www.linkedin.com/pulse/lifelock-reviews-news-report-now-published-quinn-william</a>
                            </p>
                        </div>
                    </li>
                    <li class="client">
                        <div class="client-comment">
                            <h4>Yahoo! Finance</h4>
                            <p class="previews-calendar">
                                <span><img src="../templates/requestlifelocktheme/images/page_template/client-vote.jpg"></span>
                                <span><i class="fa fa-calendar"></i> December 19, 2015</span>
                            </p>
                            <p class="description comment read-more-text">
                                <a href="http://finance.yahoo.com/news/lifelock-review-scam-success-065800563.html"  target="_blank" >http://finance.yahoo.com/news/lifelock-review-scam-success-065800563.html</a>
                            </p>
                        </div>
                    </li>
                    <li class="client">
                        <div class="client-comment">
                            <h4>Super Money</h4>
                            <p class="previews-calendar">
                                <span><img src="../templates/requestlifelocktheme/images/page_template/client-vote.jpg"></span>
                                <span><i class="fa fa-calendar"></i> December 19, 2015</span>
                            </p>
                            <p class="description comment read-more-text">
                                <a href="https://www.supermoney.com/reviews/credit-reporting/lifelock"  target="_blank" >https://www.supermoney.com/reviews/credit-reporting/lifelock</a>
                            </p>
                        </div>
                    </li>
                    <li class="client">
                        <div class="client-comment">
                            <h4>Identity Theft Labs</h4>
                            <p class="previews-calendar">
                                <span><img src="../templates/requestlifelocktheme/images/page_template/client-vote.jpg"></span>
                                <span><i class="fa fa-calendar"></i> December 19, 2015</span>
                            </p>
                            <p class="description comment read-more-text">
                                <a href="http://www.identitytheftlabs.com/lifelock-review/"  target="_blank" >http://www.identitytheftlabs.com/lifelock-review/</a>
                            </p>
                        </div>
                    </li>
                    <li class="client">
                        <div class="client-comment">
                            <h4>In Home Safety Guide</h4>
                            <p class="previews-calendar">
                                <span><img src="../templates/requestlifelocktheme/images/page_template/client-vote.jpg"></span>
                                <span><i class="fa fa-calendar"></i> December 19, 2015</span>
                            </p>
                            <p class="description comment read-more-text">
                                <a href="https://www.inhomesafetyguide.org/lifelock-review/"  target="_blank" >https://www.inhomesafetyguide.org/lifelock-review/</a>
                            </p>
                        </div>
                    </li>
                </ul>   
			        </div>
			    </div>
			  <?php endwhile;?>
</div>
<script type="text/javascript" src="https://code.jquery.com/jquery.min.js"></script>
<script type="text/javascript">
  $(document).ready(function(){
    var totalReviews = $('#data_all').attr('data-total');
    //data All
    var rating_5 = $('#data_all').attr('data-5');
    var rating_4 = $('#data_all').attr('data-4');
    var rating_3 = $('#data_all').attr('data-3');
    var rating_2 = $('#data_all').attr('data-2');
    var rating_1 = $('#data_all').attr('data-1');

    var show_percent_5 = parseInt(rating_5) / parseInt(totalReviews) * 100;
    var show_percent_4 = parseInt(rating_4) / parseInt(totalReviews) * 100;
    var show_percent_3 = parseInt(rating_3) / parseInt(totalReviews) * 100;
    var show_percent_2 = parseInt(rating_2) / parseInt(totalReviews) * 100;
    var show_percent_1 = parseInt(rating_1) / parseInt(totalReviews) * 100;

    $('#star0 #fiveStars').append(rating_5);
    $('#star1 #fiveStars').append(rating_4);
    $('#star2 #fiveStars').append(rating_3);
    $('#star3 #fiveStars').append(rating_2);
    $('#star4 #fiveStars').append(rating_1);

    $('#star0 .graph .show').css({'width': show_percent_5 + '%'});
    $('#star1 .graph .show').css({'width': show_percent_4 + '%'});
    $('#star2 .graph .show').css({'width': show_percent_3 + '%'});
    $('#star3 .graph .show').css({'width': show_percent_2 + '%'});
    $('#star4 .graph .show').css({'width': show_percent_1 + '%'});
  });
</script>
<?php get_footer();?>
