<?php 
/*
 Template Name: Homepage
*/
 get_header(); ?>

    <div id="primary">
        
        <div id="content" role="main" class="homepage">

            <?php while ( have_posts() ) : the_post(); ?>
                <?php  
                $id_page = get_the_ID();
                $id_code = '134';
                $get_post_code = new WP_Query( array( 'post_type' => 'Code','posts_per_page'=> 1,) );
                while ( $get_post_code->have_posts() ) : $get_post_code->the_post();
                    $id_code = get_the_ID();
                endwhile;
                $max_discount = get_post_meta($id_code, 'discount', true);
                $max_term = get_post_meta($id_code, 'term', true);

            ?>
            <div class="section1">
                <div class="container">
                
                    <div id="button_header" class="text-select">View All LifeLock Promo Codes</div>

                <div class="col-md-7 col-sm-6 left_homepage">
                    <?php the_field('home-section1', $id_page); ?>
                </div>
                
                <div id="shields" class="col-md-5 col-sm-6 right_homepage">
                    <div class="homeTopLeft fr re_fl">
                        <div class="custom">
                            <div class="mascot">
                                <div class="content">
                                    <div class="box autoWidth">
                                        <div id="shield_life_lock" class="fr shield <?php if(is_page('home')){ echo"page_home2"; } ?>" data="<?php echo render_url($id_code, $id_page); ?>">
                                            <h3 style="text-align: center;"><strong><span class="title_shield" style="font-size: 15pt;" >BEST OFFER:</span></strong></h3>
                                            <div id="cl-effect-5" class="cl-effect-5">
                                                <h2><a href="<?php echo render_url($id_code, $id_page); ?>">                                             
                                                            <span style="color: #ffff66;" data-hover="LLC3015"> <?php the_field('code', $id_code); ?></span></a></h2></div>
                                            
                                                <p class="text_shield" style="text-align: center;font-weight: 700;">
                                                
                                                    <span style="font-size: 18pt;">
                                                            <span style="font-size: 18pt;"><?php echo $max_discount; ?>% off
                                                            </span>
                                                        
                                                    </span>
                                                
                                                </p>

                                                <!-- <p class="text_shield" style="text-align: center;font-weight: 700;"><strong><span style="font-size: 19pt;">+</span></strong></p> -->

                                                <p class="text_shield" style="text-align: center;font-weight: 700;">
                                                
                                                    <span style="font-size: 19pt;">
                                                        
                                                            <span style="font-size: 14pt;">
                                                                first year* <br> +  Free <br> Shredder
                                                            </span>
                                                        
                                                    </span>
                                                
                                                <br />
                                        </div>
                                        <div class="holder"><img src="<?php bloginfo('template_directory'); ?>/image/layer1-hand.png" alt="Life Lock promo code" /></div>
                                    </div>
                                </div>
                            </div>
                            <div class="homeTopRight">
                                <h2 class="red"><a class="button" style="padding-left: 50px; padding-right: 0px;" title="Enroll using Promo Code" href="<?php echo render_url($id_code, $id_page); ?>">Enroll using Promo Code</a></h2>
                                <h2 class="red"><a class="button" style="padding-left: 50px; padding-right: 0px;" title="Get Pricing" href="/pricing">Get Pricing</a></h2></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div id="homepage" class="section2">
                <div class="container">
                    <?php


                        $loop = new WP_Query( array( 'post_type' => 'Code','posts_per_page'=> 5) );

                        foreach ($loop->posts as $key => $post) {
                            $order = get_field('order', $post->ID);
                            $post->order = $order;
                        }

                        usort($loop->posts, function($a, $b) {
                            return ($a->order <= $b->order);
                        });

                        $i=1;
                        while ( $loop->have_posts() ) : $loop->the_post();
                            if($i == 1) {
                                $class_sd = 'sdone';
                            } elseif ($i == 2 ) {
                                $class_sd = 'sdtwo';
                            } elseif ($i == 3) {
                                $class_sd = 'sdthree';
                            } elseif ($i == 4) {
                                $class_sd = 'sdfour';
                            } elseif ($i == 5) {
                                $class_sd = 'sdfive';
                            } else {
                                $class_sd = 'sdfive';
                            }
                    ?>

                        <div class="sheld <?php echo $class_sd; ?>">
                            <div class="sheld_children_homepage content-shel" data_child="<?php echo render_url(get_the_ID(), $id_page); ?>">
                                <p>
                                    <?php the_field('name'); ?>
                                </p>
                                <p>
                                    <a class="url_sheld" href="<?php echo render_url(get_the_ID(), $id_page); ?>">
                                        <?php the_field('code'); ?>
                                    </a>
                                    <?php the_field('promotion'); ?>
                                </p>
                            </div>
                        </div>
                    <?php
                            $i++;
                        endwhile;
                    ?>
                </div>
            </div>
        </div>

        <div class="text_bottom_home">
            <p>*Terms Apply</p>
        </div>

        <div class="section3">
            <div class="container">
                <?php the_field('home-section3', $id_page); ?>
            </div>
        </div>

        <div class="section4">
            <div class="container">
                <?php the_field('home-section4', $id_page); ?>
            </div>
        </div>
    </div>

    <?php endwhile; // end of the loop. ?>

        </div>
        <!-- #content -->
        </div>
        <!-- #primary -->

        <?php get_footer(); ?>