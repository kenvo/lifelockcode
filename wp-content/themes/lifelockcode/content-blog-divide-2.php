<?php 
	if (is_single()) {
		?>
			<div class="col-md-6 col-sm-12 col-xs-12 garung_custom_column">
				<article id="post-<?php the_ID();?>" <?php post_class();?>>
					<div id="garung-blog">
						<header class="entry-header garung-header">
							<?php
							$my_date = the_date('', '<p>', '</p>', false);
							?>
							<span class="catItemDateCreated"><?php echo $my_date; ?></span>
							<h2><a href="<?php the_permalink();?>" title="<?php the_title_attribute();?>"><?php the_title();?></a></h2>
							<p>Writen by <?php echo get_the_author(); ?></p>
						</header><!-- .entry-header -->
						<div class="post-thumnai">
							<?php
							// Post thumbnail.
							echo get_the_post_thumbnail($_post->ID, 'medium');
							?>
								</div>
								<div class="entry-content">
									<?php
							/* translators: %s: Name of current post */
							the_content(sprintf(
							    __('Read more...', 'lifelockcode')));

							wp_link_pages(array(
							    'before'      => '<div class="page-links"><span class="page-links-title">' . __('Pages:', 'lifelockcode') . '</span>',
							    'after'       => '</div>',
							    'link_before' => '<span>',
							    'link_after'  => '</span>',
							    'pagelink'    => '<span class="screen-reader-text">' . __('Page', 'lifelockcode') . ' </span>%',
							    'separator'   => '<span class="screen-reader-text">, </span>',
							));
							?>
						</div><!-- .entry-content -->
						<footer class="entry-footer">
							<div class="catItemCategory">
								<?php
								$args = array(
								    'orderby' => 'name',
								    'parent'  => 0,
								);
								$categories = get_categories($args);
								$publish = 'Published in ';
								$i = 0;
								$count_cat = count($categories);
								foreach ($categories as $key => $category) {
								    $publish .= '<a href="'. get_category_link($category->term_id) . '">' . $category->name . '</a>';
								    if($i < ($count_cat- 1)) {
								    	$publish .= ', ';
								    }
								    $i++;
								}
								echo $publish;
								?>
							</div>
							<div class="catItemTagsBlock">
								<p>Tagged under</p>
								<?php
								global $post;

								if (get_the_tags($post->ID) == true) {
									foreach (get_the_tags($post->ID) as $tag) {
								   		echo '<li><a href="' . get_tag_link($tag->term_id) . '">' . $tag->name . '</a></li>';
									}
								}
								?>
							</div>
							<div class="garung_cat_readmore">
								<a class="more-links" href="<?php echo get_permalink(); ?>">Read more ...</a>
							</div>
						</footer><!-- .entry-footer -->
						<?php
						// Author bio.
						if (is_single() && get_the_author_meta('description')):
						    get_template_part('author-bio');
						endif;
						?>
					</div>
				</article><!-- #post-## -->
			</div>
		<?php
	}else{
		?>
			<div class="col-md-3 col-sm-6 col-xs-12 garung_custom_column">
				<div id="garung-blog">
					<div class="post-thumnai thumbnail_blog">
						<a href="<?php the_permalink();?>" title="<?php the_title_attribute();?>">
							<?php
								// Post thumbnail.
								if (get_the_post_thumbnail($_post->ID, 'medium')) {

									echo get_the_post_thumbnail($_post->ID, 'medium');
								}else{
									echo "<img src='". get_stylesheet_directory_uri() ."/image/blog_281.jpg'>";
								}
								// echo get_the_post_thumbnail($_post->ID, 'medium');
							?>
						</a>
					</div>

					<header style="height: 90px;" class="entry-header garung-header header_render">
						<p><a class="title_blog" style="color: black;" href="<?php the_permalink();?>" title="<?php the_title_attribute();?>"><?php the_title();?></a></p>
					</header><!-- .entry-header -->
				</div>
			</div>
		<?php
	}

?>