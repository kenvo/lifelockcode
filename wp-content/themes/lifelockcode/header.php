<!DOCTYPE html>
<html >
<head>

	   <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">

	   <link rel="sicon" type="image/x-icon" href="<?php echo get_site_icon_url( 32 ); ?>"/>
	   <link rel="shortcut icon" type="image/x-icon" href="<?php echo get_site_icon_url( 32 ); ?>"/>
	   
	<?php wp_head();?>
	<script type="text/javascript">
		if(screen.width > 1200) {
			document.write('<meta name="viewport" content="width=1200">');
		} else {
			document.write('<meta name="viewport" content="width=device-width, initial-scale=1.0">');
		}
	</script>
	<script>
		jQuery(document).ready(function($) {
			
			  $('.a_tag').click(function(){  
			    $('html, body').animate({
			        scrollTop: $( $(this).attr('data-href') ).offset().top
			    }, 400);
			    return false;
			  });

			  jQuery(".custom").on('click', function() {
			      jQuery('html, body').animate({
			        scrollTop: jQuery('#shields').offset().top
			      }, 800, function(){

			      });
			  });
			
		});
	</script>

	<script>
		(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

		ga('create', 'UA-105609945-1', 'auto');
		ga('send', 'pageview');
	</script>

	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-92588913-1"></script>
	<script>
	window.dataLayer = window.dataLayer || [];
	function gtag(){dataLayer.push(arguments);}
	gtag('js', new Date());

	gtag('config', 'UA-92588913-1');
	</script>

</head>
<body <?php body_class();?>>
	
	<!-- Google Tag Manager -->
	<!-- <noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-MBZF2X"
	height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
	<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
	new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
	j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
	'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
	})(window,document,'script','dataLayer','GTM-MBZF2X');</script> -->
	<!-- End Google Tag Manager -->
	<div class="loading"></div>

<?php

    $id_page = get_the_ID();
    $id_code = '134';
    $get_post_code = new WP_Query( array( 'post_type' => 'Code','posts_per_page'=> 1,) );
    while ( $get_post_code->have_posts() ) : $get_post_code->the_post();
        $id_code = get_the_ID();
    endwhile;
    $max_discount = get_post_meta($id_code, 'discount', true);
    $max_term = get_post_meta($id_code, 'term', true);
?>
<div id="page" class="hfeed site">
		<?php $site_icon = get_option( 'title_tagline' ); 
			// echo "<pre>";
			// var_dump($site_icon);
			// die();

		?>
		
		<header id="masthead"  class="site-header <?php if(current_user_can('administrator')){ echo "amdin_header"; } ?>" role="banner">
			<div class="site-branding container <?php if(is_page('promo-codes-learn-more')){ echo "page_learn_more"; } ?>">
			<div class="logo col-md-6">
				<div class="img-responsive"><?php lifelockcode_the_custom_logo();?></div>
				<aside id="text-3" class="widget widget_text">			
					<div class="textwidget">
						<div class="custom">
							<p class="date" style="padding-bottom: 0px;margin-bottom: 10px;"><span style="text-align: center; font-size: 12pt;"><strong><?php //the_modified_date('F Y');?><?php echo date_i18n('F Y'); ?> - LifeLock Promo Code</strong></span></p>
							<h2 class="pc_promo" style="font-size: 18px;">
								<strong>
									<span style="text-align: center; font-size: 12pt;">
										<p class="text_header_kc">
											<a href="https://lifelocktrk.com/?a=107&c=82&s1=dmn&s2=llpc&s3=LLCSHRED&s4=www.llpromocodes.com&s5=home-2" title="Promo Codes"><?php //the_field('name', '169');?>
											<?php the_field('code', $id_code); ?>
											</a>
										</p>
									</span>
								</strong>

								<div class="text_code" style="margin-top: 1px;text-align: center;">
                                    <span style="font-size: 19pt;">
                                        <strong style="text-align: center;">
                                            <span style="font-size: 11pt;">
                                            Save <?php echo $max_discount; ?>% first year* + Free Shredder *Terms Apply
                                            </span>
                                        </strong>
                                    </span>
                                </div>
							</h2>

							<h2 class="mobile_promo" style="font-size: 18px;">
								<strong>
									<span style="text-align: center; font-size: 12pt;">
										<a href="<?php echo render_url($id_code, $id_page); ?>" title="Promo Codes"><?php the_field('code', $id_code); ?> - Save <?php echo $max_discount; ?>%
										</a><br>
									</span>
								</strong>

								<div style="margin-top: 10px;text-align: center;">
                                    <span style="font-size: 19pt;">
                                        <strong style="text-align: center;">
                                            <span style="font-size: 14pt;">
                                            Top LifeLock Promo Code <br> *Terms Apply
                                            </span>
                                        </strong>
                                    </span>
                                </div>
							</h2>

						</div>
					</div>
				</aside>			
			</div>
			<div class="main-menu col-md-6">
								<nav class="navbar navbar-default navbar-fixed-top" role="navigation">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
			</div>
			<div id="navbar" class="navbar-collapse collapse" aria-expanded="false" style="height: 1px;">
				<ul class="nav navbar-nav">
								<?php lifelockcode_the_custom_menu('primary-menu');?>
								<script type="text/javascript">
									// jQuery(document).ready(function(){
										var html = '<li id="menu-item-98" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-98"><a href="<?php echo render_url($id_code, $id_page); ?>"><i class="_mi _before dashicons dashicons-lock" aria-hidden="true"></i><span><p>Lifelock.com</p><p>(800) 948-0498</p></span></a></li>';
										jQuery('#menu-main-menu').append(html);
									// });
								</script>
					
				</ul>
			</div><!--/.nav-collapse -->
	</nav>
				</div>
			</div><!-- .site-branding -->
		</header><!-- .site-header -->
		<div class="text_button_header">
			<p>Affiliate Disclosure: We receive compensation from LifeLock for purchases made through links on this site.</p>
		</div>
	<div id="content" class="site-content <?php if(current_user_can('administrator')){ echo "amdin_content"; } ?>">

<?php	//wp_die("<div><strong>Current template:</strong>" . get_current_template( true ) . "</div>");  ?>