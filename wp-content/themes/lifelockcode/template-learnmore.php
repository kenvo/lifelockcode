<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
<?php
/*
Template Name: Learn More
 */
get_header();

?>
<div id="main-wrapper">
	<div class="pagesCont">
	<?php while( have_posts() ) : the_post(); ?>

		<div id="system-message-container"></div>
			<div class="item-page" itemscope="" itemtype="http://schema.org/Article">
				<meta itemprop="inLanguage" content="en-GB">
				<div itemprop="articleBody">
					<div class="innerCont w1354">
						<div class="largeTitle">
							<h1>Identity Theft</h1>
						</div>
						<div class="box-shadow">
							<h2><?php echo get_the_title(); ?></h2>
							<img style="margin: 5px; float: left;" title="Identity Theft" src="<?php echo get_the_post_thumbnail_url(); ?>" alt="LifeLock coupon" width="161" height="139">
							<?php echo the_content(); ?>

							<div class="error">
								<?php  
					                $id_page = get_the_ID();
					                $id_code = '134';
					                $get_post_code = new WP_Query( array( 'post_type' => 'Code','posts_per_page'=> 1,) );
					                while ( $get_post_code->have_posts() ) : $get_post_code->the_post();
					                    $id_code = get_the_ID();
					                endwhile;
					                $max_discount = get_post_meta($id_code, 'discount', true);
					                $max_term = get_post_meta($id_code, 'term', true);
					            ?>
								<strong>
									<span>
										<p style="font-size: 14pt; font-weight: bold; margin-left: 13px;">Enroll now using Promo Code <a href="<?php echo render_url($id_code, $id_page); ?>" title="LLC1030" style="color:#999966;">LLC1030</a></p>
									</span>
									<span>
										<p style="font-size: 14pt; font-weight: bold;">Save <span><?php echo $max_discount; ?>%</span> off All Plans</p>
									</span>
								</strong>
							</div>
						</div>

					</div>


				</div>
			</div>
		<div class="sidebar-landingpage homeTopLeft fr re_fl"></div>
	<?php endwhile; ?>
	</div>
</div>
<?php get_footer();?>